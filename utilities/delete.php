<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Delete Item</title>
</head>
<body>

<?php
// Include your database connection file
include_once 'db_connection.php';

// Check if the item_id is set in the URL
if (isset($_GET['item_id']) && is_numeric($_GET['item_id'])) {
    $item_id = $_GET['item_id'];

    // Fetch item details from the database
    $sql = "SELECT * FROM inventory WHERE item_id = $item_id";
    $result = mysqli_query($connection, $sql);

    if ($result && mysqli_num_rows($result) > 0) {
        $row = mysqli_fetch_assoc($result);
        ?>
        <h2>Delete Item</h2>
        <p>Are you sure you want to delete the item with ID <?php echo $row['item_id']; ?>?</p>
        <form action="delete-process.php" method="post">
            <input type="hidden" name="item_id" value="<?php echo $row['item_id']; ?>">
            <input type="submit" value="Delete">
        </form>
        <?php
    } else {
        echo "Item not found.";
    }

    // Free result set
    mysqli_free_result($result);

} else {
    echo "Invalid item ID.";
}

// Close the database connection
mysqli_close($connection);
?>

</body>
</html>
