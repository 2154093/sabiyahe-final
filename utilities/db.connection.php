<?php
$servername = "127.0.0.1:3306";
$username = "root";
$password = "";
$database = "webtechdb";

// Create connection
$connection = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$connection) {
    die("Connection failed: " . mysqli_connect_error());
}
?>