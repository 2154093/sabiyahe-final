<?php
// Include your database connection file
include_once 'db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Check if item_id is set and numeric
    if (isset($_POST['item_id']) && is_numeric($_POST['item_id'])) {
        $item_id = $_POST['item_id'];

        // Delete the item from the database
        $sql = "DELETE FROM inventory WHERE item_id = $item_id";

        if (mysqli_query($connection, $sql)) {
            echo "Item deleted successfully.";
        } else {
            echo "Error deleting item: " . mysqli_error($connection);
        }

    } else {
        echo "Invalid item ID.";
    }
}

// Close the database connection
mysqli_close($connection);
?>