const express = require('express');
const router = express.Router();
const db = require('../../routes/db-config');

// Endpoint to retrieve available items in the inventory
router.get('/inventory', async (req, res) => {
    try {
        // Fetch items from the database where the quantity is greater than zero (assuming quantity field)
        const availableItems = await db.query('SELECT * FROM items WHERE quantity > 0');

        if (availableItems.length === 0) {
            return res.status(404).json({ status: 'success', message: 'No available items in the inventory' });
        }

        return res.status(200).json({ status: 'success', data: availableItems });
    } catch (error) {
        console.error('Error retrieving available items:', error);
        return res.status(500).json({ status: 'error', error: 'Internal server error' });
    }
});

module.exports = router;
