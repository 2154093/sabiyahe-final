const db = require('../../routes/db-config');

const register = async (req, res) => {
    const { username, password: Npassword, fullname } = req.body;

    if(!username || !Npassword || !fullname) {
        return res.json({ status: 'error', error: 'Please enter fullname, password, or username' });
    } else {
        console.log(username);
        db.query('SELECT username FROM users WHERE username = ?', [username], async (err, result) => {
            if (err) throw err;

            if(result[0]) {
                return res.json({ status: 'error', error: 'username is already registered' });
            } else {
                console.log(Npassword);
                db.query('INSERT INTO users SET ?', { username: username, password: Npassword, full_name: fullname }, (error, results) => {
                    if (error) throw error;
                    return res.json({ status: 'success', success: 'Registered successfully' });
                })
            }
        });
    }
};

module.exports = register;