const jwt = require('jsonwebtoken');
const db = require('../../routes/db-config');

const login = async (req, res) => {
    const { username, password } = req.body;

    if (!username || !password) {
        return res.json({ status: 'error', error: 'Please enter username or password' });
    } else{
        db.query('SELECT * FROM users WHERE username = ?', [username], async (err, result) => {
            if (err) throw err;

            if(!result.length || password !== result[0].password) {
                return res.json({ status: 'error', error: 'Incorrect username or password' });
            } else {
                const token = jwt.sign({ id: result[0].id }, process.env.JWT_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES
                })
                const cookieOptions = {
                    expiresIn: new Date(Date.now() + process.env.COOKIE_EXPIRES * 24 * 60 * 60 * 1000)
                }
                res.cookie('userRegistered', token, cookieOptions);
                return res.json({ status: 'success', success: 'user has been logged in'});
            }
        })
    }
};

module.exports = login;