// viewAvailableItems.js

const fetch = require('node-fetch');

async function fetchAvailableItems() {
    try {
        const response = await fetch('http://localhost:3000/inventory'); // Update the URL with your actual server URL
        const data = await response.json();

        if (response.ok) {
            displayAvailableItems(data.data);
        } else {
            console.error('Error:', data.error);
            displayError('Failed to fetch available items');
        }
    } catch (error) {
        console.error('Error fetching available items:', error);
        displayError('Failed to fetch available items');
    }
}

function displayAvailableItems(items) {
    if (items.length > 0) {
        items.forEach(item => {
            console.log(`${item.name} - Quantity: ${item.quantity}`);
        });
    } else {
        console.log('No available items in the inventory.');
    }
}

function displayError(message) {
    console.error(`Error: ${message}`);
}

// Fetch and display available items
fetchAvailableItems();
