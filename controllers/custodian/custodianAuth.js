const express = require('express');
const router = express.Router();

const custodianInventory = require('./custodianInventory');

// create
router.post('/custodian/insert', (request, response) => {
    const { name } = request.body;
    const db = custodianInventory.getInventoryInstance();

    const result = db.insertNewItem(name);

    result
        .then(data => response.json({ data : data }))
        .catch(err => { console.log(err)})
});

// read
router.get('/custodian/getAll', (request, response) => {
    const db = custodianInventory.getInventoryInstance();

    const result = db.getAllData();

    result
        .then(data => response.json({ data : data }))
        .catch(err => console.log(err));
});

// update
router.patch('/custodian/toggle/:id', (request, response) => {
    const { id } = request.params;
    const db = custodianInventory.getInventoryInstance();

    const result = db.toggleAvailabilityById(id);

    result
        .then(data => response.json({ success: data }))
        .catch(err => console.log(err));
});

// delete
router.delete('/custodian/delete/:id', (request, response) => {
    const { id } = request.params;
    const db = custodianInventory.getInventoryInstance();

    const result = db.deleteRowById(id);

    result
        .then(data => response.json({ success : data }))
        .catch(err => console.log(err));
})

module.exports = router;