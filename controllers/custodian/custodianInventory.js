const mysql = require('mysql');
const dotenv = require('dotenv').config();
let instance = null;

const db = require('../../routes/db-config');

class CustodianInventory{
    static getInventoryInstance() {
        return instance ? instance : new CustodianInventory();
    }

    //read the data from the inventory table of webtechdb
    // '/custodian/getAll'
    async getAllData() {
        try {
            const response = await new Promise((resolve, reject) => {
                const query = 'SELECT * FROM inventory;';

                db.query(query, (err, results) => {
                    if (err) reject(new Error(err.message));
                    resolve(results);
                })
            });

            // console.log(response);
            return response;

        } catch (error) {
            console.log(error);
        }
    }

    // create/insert the data into the inventory table of webtechdb
    // '/custodian/insert'
    async insertNewItem(name) {
        try {
            const insertId = await new Promise((resolve, reject) => {
                const query = 'INSERT INTO inventory (item_name, availability) VALUES (?, ?);';

                db.query(query, [name, 1], (err, result) => {
                    if (err) reject(new Error(err.message));
                    resolve(result.insertId);
                });
            });

            return {
                id: insertId,
                name: name,
                availability: 1, // Assuming you want to return the default value for availability
            };
        } catch (error) {
            console.log(error);
        }
    }

    // update/patch the availability of an item
    // '/custodian/toggle/:id'
    async toggleAvailabilityById(id) {
        try {
            id = parseInt(id, 10);
            const response = await new Promise((resolve, reject) => {
                const query = 'UPDATE inventory SET availability = 1 - availability WHERE item_id = ?';
    
                db.query(query, [id], (err, result) => {
                    if (err) reject(new Error(err.message));
                    resolve(result.affectedRows);
                });
            });
    
            return response === 1 ? true : false;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    // delete an item in the inventory table of webtechdb
    // '/custodian/delete:id'
    async deleteRowById(id) {
        try {
            id = parseInt(id, 10);
            const response = await new Promise((resolve, reject) => {
                const query = 'DELETE FROM inventory WHERE item_id = ?';

                db.query(query, [id], (err, result) => {
                    if (err) reject(new Error(err.message));
                    resolve(result.affectedRows);
                });
            });

            return response === 1 ? true : false;
        } catch (error) {
            console.log(error);
            return false;
        }
    }
}

module.exports = CustodianInventory;