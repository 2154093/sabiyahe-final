const formLogin = document.getElementById('form-login');

formLogin.addEventListener('submit', (event) => {
    const usernameLogin = document.getElementById('username-login').value;
    const passwordLogin = document.getElementById('password-login').value;

    const login = {
        username: usernameLogin,
        password: passwordLogin,
    };

    fetch('/api/login', {
        method: 'POST',
        body: JSON.stringify(login),
        headers: {
            'Content-Type': 'application/json',
        },
    }).then(res => res.json())
        .then(data => {
            const errorLogin = document.getElementById('error-login');
            const successLogin = document.getElementById('success-login');

            if (data.status === 'error') {
                successLogin.style.display = 'none';
                errorLogin.style.display = 'block';
                errorLogin.innerText = data.error;
            } else {
                // redirect
                errorLogin.style.display = 'none';
                successLogin.style.display = 'block';
                successLogin.innerText = data.success;
            }
        });
});