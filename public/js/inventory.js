const custodianInventory = document.getElementById('inventory');

document.addEventListener('DOMContentLoaded', function () {
    fetch('api/custodian/getAll')
        .then(response => response.json())
        .then(data => loadHTMLTable(data['data']));

});

document.querySelector('table tbody').addEventListener('click', function(event) {
    if (event.target.className === 'delete-row-btn') {
        deleteRowById(event.target.dataset.id);
    }
    if (event.target.className === 'edit-availability-btn') {
        toggleAvailabilityById(event.target.dataset.id);
    }
});


function deleteRowById(id) {
    fetch('/api/custodian/delete/' + id, {
        method: 'DELETE'
    })
    .then(response => response.json())
    .then(data => {
        if (data.success) {
            const deletedRow = document.getElementById('row-' + id);
            if (deletedRow) {
                deletedRow.remove();
            }
        }
    })
    .catch(error => console.log(error));
}

function toggleAvailabilityById(id) {
    fetch(`/api/custodian/toggle/${id}`, {
        method: 'PATCH'
    })
        .then(response => response.json())
        .then(data => {
            if (data.success) {
                // Reload the table after successful toggle
                fetch('api/custodian/getAll')
                    .then(response => response.json())
                    .then(data => loadHTMLTable(data['data']));
            }
        })
        .catch(error => console.log(error));
}

const addBtn = document.querySelector('#add-name-btn');
addBtn.onclick = function () {
    const nameInput = document.querySelector('#name-input');
    const name = nameInput.value;
    nameInput.value = '';

    fetch('/api/custodian/insert', {
        headers: {
            'Content-type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify({ name: name })
    })
        .then(response => response.json())
        .then(data => insertRowIntoTable(data['data']))
        .catch(error => console.log(error));
}

function insertRowIntoTable(data) {
    const table = document.querySelector('table tbody');

    const availabilityText = data.availability === 1 ? 'Available' : 'Unavailable';

    const newRow = `
        <tr>
            <td>${data.item_id}</td>
            <td>${data.item_name}</td>
            <td>${availabilityText}</td>
            <td><button class='edit-availability-btn' data-id='${data.item_id}'>Edit Availability</td>
            <td><button class='delete-row-btn' data-id='${data.item_id}'>Delete</td>
        </tr>
    `;

    table.innerHTML += newRow;
    location.reload();
}

function loadHTMLTable(data) {
    const table = document.querySelector('table tbody');

    if (data.length === 0) {
        table.innerHTML = "<tr><td class='no-data' colspan='6'>No Data</td></tr>";
        return;
    }

    let tableHtml = '';

    data.forEach(function ({ item_id, item_name, item_type, availability }) {
        const availabilityText = availability === 1 ? 'Available' : 'Unavailable';

        tableHtml += `<tr id='row-${item_id}'>`;
        tableHtml += `<td>${item_id}</td>`
        tableHtml += `<td>${item_name}</td>`
        tableHtml += `<td>${availabilityText}</td>`
        tableHtml += `<td><button class='edit-availability-btn' data-id='${item_id}'>Change Availability</td>`
        tableHtml += `<td><button class='delete-row-btn' data-id='${item_id}'>Delete</td>`
        tableHtml += '</tr>'
    });

    table.innerHTML = tableHtml;
}