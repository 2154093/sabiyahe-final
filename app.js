const express = require('express');
const db = require('./routes/db-config');
const cookie = require('cookie-parser');
const cors = require('cors');
const port = 5000;

// express app
const app = express();

// middleware and static files
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/css', express.static(__dirname + '/public/css'));

// set view engine
app.set('view engine', 'ejs');
app.set('views', './views');
app.use(cookie());
app.use(cors());
// Parse JSON bodies (as sent by API clients)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// connect to mysql
db.connect( (error) => {
    if(error){
        console.log(error);
    } else {
        console.log('MYSQL Connected!')
    }
})

// routes
app.use('/', require('./routes/pages'));
app.use('/api', require('./controllers/studentAuth/auth'));
app.use('/api', require('./controllers/custodian/custodianAuth'));

app.listen(port, () => {
    console.log(`server started on port ${port}`);
});