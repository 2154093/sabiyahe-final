const express = require('express');
// const loggedIn = require('../controllers/loggedin');
// const logout = require('../controllers/logout');
const router = express.Router();

// Student Routes
// Authentication
router.get('/register', (req, res) => {
    res.sendFile('register.html', { root:'./public' });
});
router.get('/login', (req, res) => {
    res.sendFile('login.html', { root:'./public' });
});

// Custodian Routes
// Inventory
router.get('/custodian', (req, res) => {
    res.sendFile('custodianHome.html', { root:'./public' });
});

module.exports = router;