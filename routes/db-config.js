const mysql = require('mysql');
const dotenv = require('dotenv').config();
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'webtechdb'
})

module.exports = db;